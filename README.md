# boost-example

#### 介绍
boost库实践

#### Boost库使用简易教程

1.  安装Boost库
    1. 首先从[Boost官网](https://www.boost.org/)下载Boost库（本仓库使用1.77.0）
    2. 解压Boost库到任意路径，将该路径设置为`BOOST_ROOT`环境变量（本例中为`/home/user/boost_1_77_0`）
    2. 进入`${BOOST_ROOT}`，执行`./bootstrap.sh`；此步骤为构建`b2`构建工具；
    3. 执行`./b2 install`安装所有Boost库组件
2.  在CMake中使用Boost库
    1. 在CMakeLists.txt中添加如下
    ```CMake
    # 来自FindBoost.cmake的例子
    # 使用header-only的Boost库

       find_package(Boost 1.77.0)
       if(Boost_FOUND)
         include_directories(${Boost_INCLUDE_DIRS})
         add_executable(foo foo.cc)
       endif()

    # 使用Boost库并导入IMPORTED对象

    find_package(Boost 1.77 REQUIRED COMPONENTS
                 date_time filesystem iostreams)
    add_executable(foo foo.cc)
    target_link_libraries(foo Boost::date_time Boost::filesystem
                          Boost::iostreams)

    # 仅使用Boost静态库，并导入Boost头文件

    set(Boost_USE_STATIC_LIBS        ON)  # 仅使用静态库
    set(Boost_USE_DEBUG_LIBS         OFF) # 
    set(Boost_USE_RELEASE_LIBS       ON)  # 忽略Debug库，仅查找Release级别库
    set(Boost_USE_MULTITHREADED      ON)
    set(Boost_USE_STATIC_RUNTIME    OFF)
    find_package(Boost 1.77.0 COMPONENTS date_time filesystem system)
    if(Boost_FOUND)
      include_directories(${Boost_INCLUDE_DIRS})
      add_executable(foo foo.cc)
      target_link_libraries(foo ${Boost_LIBRARIES})
    endif()
    ```
    2. 需要注意的几个Boost相关CMake变量：
        1. `Boost_INCLUDE_DIRS`：Boost头文件路径，用于`include_directories`；
        2. `Boost_LIBRARIES`：Boost库文件路径，用于`target_link_libraries`；
3.  待补充

